<?php
namespace Kartio\ClickerHeroes;


class HeroNumber {

    protected $number;
    protected $symbol;


    public function __construct($absurdNumber) {

        list($this->number, $this->symbol) = $this->parseNumber($absurdNumber);

    }

    public function __toString() {
        return $this->getSmartValue();
    }

    /**
     * Returns short value for smaller numbers and scientific notation for big
     */
    public function getSmartValue() {
        $symbol = null;

        $short = $this->getShortValue($symbol);

        if ($symbol == '*') {
            return $this->getScientificNotation();
        }
        else {
            return $short;
        }

    }

    public function getRealValue() {

        if ($this->symbol === '') return $this->number;

        $symbols = 'KMBTqQsSONdUD!@#$%^&*';

        $factor = strpos($symbols, $this->symbol);

        if ($factor === false) throw new \OutOfRangeException("Unknown symbol.");

        return gmp_strval(gmp_mul($this->number, gmp_pow(1000, $factor + 1)));

    }

    public function getShortValue(&$usedSymbol = null) {

        $realNumber = $this->getRealValue();

        // If no symbol is needed just return the actual number
        if (gmp_cmp($realNumber, 99999) < 1) return $realNumber;

        $symbols = 'KMBTqQsSONdUD!@#$%^&*';
        $lastSymbol = substr($symbols, -1);

        $quotient = $realNumber;
        foreach (range(0, strlen($symbols)) as $i) {

            // Check for overflow
            if ($i >= strlen($symbols)) {
                $usedSymbol = $lastSymbol;
                return gmp_strval($quotient). $lastSymbol;
            }

            $quotient = gmp_div_q($quotient, 1000);

            if (gmp_cmp($quotient, 99999) < 1) { // We have the proper symbol
                $usedSymbol = $symbols[$i];
                return gmp_strval($quotient). $symbols[$i];
            }

        }

    }

    public function getScientificNotation() {
        $realNumber = $this->getRealValue();

        if (gmp_cmp($realNumber, 99999) < 1) return $realNumber;

        preg_match('/^(\d)(\d{1,3})(.+?)$/', $realNumber, $match);

        $sn = sprintf('%s.%se%d', $match[1], $match[2], strlen($match[3]) + strlen($match[2]));

        return $sn;
    }

    protected function parseNumber($absurdNumber) {

        $shortNumberPattern        = '/^(?:(\d+)(?:,(\d+))?)\s*([KMBTqQsSONdUD!@#$%\^&*]?)$/';
        $scientificNotationPattern = '/^(\d+)(?:\.(\d+))?e(\d+)$/';
        $normalNumber              = '/^(\d+)$/';


        if (preg_match($shortNumberPattern, trim((string)$absurdNumber), $matches)) {
            $number = $matches[1] . $matches[2];
            $symbol = $matches[3];
            return [$number, $symbol];
        }
        elseif (preg_match($scientificNotationPattern, trim((string)$absurdNumber), $matches)) {
            $zeros = $matches[3] - strlen($matches[2]);
            if ($zeros < 0) throw new \UnexpectedValueException('Invalid exponent.');
            $number = $matches[1] . $matches[2] . str_repeat('0', $zeros);
            return [$number, ''];
        }
        elseif (preg_match($normalNumber, trim((string)$absurdNumber), $matches)) {
            return [$matches[1], ''];
        }
        else {
            throw new \UnexpectedValueException("Can not parse given number. Maybe you have unknown symbol?");
        }

    }

} 