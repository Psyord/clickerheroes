<?php

namespace Kartio\ClickerHeroes;

class Hero {

    /**
     * @var string
     */
    protected $name;
    /**
     * @var string
     */
    protected $baseDamage;
    /**
     * @var string
     */
    protected $baseCost;
    /**
     * @var array
     */
    protected $upgrades = [];

    protected $dpsCache = [];
    protected $levelCostCache = [];
    protected $cumulativeLevelCostCache = [];

    /**
     * @param $name
     * @param HeroNumber $baseDamage
     * @param HeroNumber $baseCost
     * @param \stdClass $upgrades
     */
    public function __construct($name, HeroNumber $baseDamage, HeroNumber $baseCost, $upgrades) {

        $this->name = $name;

        $this->baseDamage = $baseDamage->getRealValue();
        $this->baseCost   = $baseCost->getRealValue();

        $this->upgrades = $upgrades;

    }

    /**
     * @param $level
     * @return string
     */
    public function getUpgradeBonus($level) {

        $upgradeLevels = [10,25,50,75,100,125];
        $bonus = 0;

        foreach ($upgradeLevels as $ul) {
            if ($level >= $ul && isset($this->upgrades->$ul)) {
                $bonus += $this->upgrades->$ul->value;
            }
        }

        return (string) $bonus;

    }

    /**
     * Calculates DPS in the given level assuming full self-upgrades.
     * Ignores all outside factors, like achievements or group bonuses.
     * @param int $level
     * @return string
     */
    public function getDps($level) {

        if (isset($this->dpsCache[$level])) return $this->dpsCache[$level];

        // Base DPS without buffs (base*level)
        $dps = gmp_mul($this->baseDamage, $level);

        // Personal upgrades, including group buffs for self only (add 100 as 100% base)
        $bonus = ((int) $this->getUpgradeBonus($level)) + 100;

        // Bonus % added to DPS (dps*bonus/100) -> bonus is integer
        $realDps = gmp_div_q(gmp_mul($dps, $bonus), 100);

        if ($level >= 200) { // 4x and 10x only happen after 200lvl
            foreach (range(200, $level) as $point) { // Not using stepping because PHP is stupid
                if ($point >= 1000 && ($point % 1000) == 0) { // Every 1000 levels 10x damage
                    $realDps = gmp_mul($realDps, 10);
                }
                elseif ($point >= 200 && ($point % 25) == 0) { // every 25 level 4x damage starting from 200
                    $realDps = gmp_mul($realDps, 4);
                }
            }
        }

        return $this->dpsCache[$level] = gmp_strval($realDps);

    }

    /**
     * @return string Name of the Hero
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param int $level
     * @return string
     */
    public function getLevelCost($level) {

        if (isset($this->levelCostCache[$level])) return $this->levelCostCache[$level];

        //base cost * 1.07 ^ (level-1)
        // (50 * 107^n) / (100^n)

        $dps = gmp_strval(gmp_div_q(
            gmp_mul($this->baseCost, gmp_pow(107, $level - 1)),
            gmp_pow(100, $level - 1)
        ));

        return $this->levelCostCache[$level] = $dps;

    }

    /**
     * Returns cost of hero including every level from 1 to $level
     * @param int $level
     * @return string
     */
    public function getCumulativeCost($level) {

        $total = 0;

        foreach (range(1, $level) as $n) {

            $total = gmp_add($total, $this->getLevelCost($n));

        }

        return gmp_strval($total);

    }

    /**
     * Returns cost of hero including every level from 1 to $level. Including upgrade costs.
     * @param int $level
     * @return string
     */
    public function getCumulativeCostWithUpgrades($level) {

        if (isset($this->cumulativeLevelCostCache[$level])) return $this->cumulativeLevelCostCache[$level];
        $total = 0;

        foreach (range(1, $level) as $n) {

            $levelCost = $this->getLevelCost($n);

            if (in_array($n, [10,25,50,75,100,125]) && isset($this->upgrades->$n->cost)) {
                $levelCost = gmp_add($levelCost, $this->upgrades->$n->cost);
            }

            $total = gmp_add($total, $levelCost);

        }

        return $this->cumulativeLevelCostCache[$level] = gmp_strval($total);

    }

    /**
     * Calculates relative number how much benefit this level gives for hero given the cost. Lower is better.
     * @param int $level
     * @return float
     */
    public function getEfficiency($level) {

        $dps = $this->getDps($level);
        // No real reason to use cumulative
        $cost = $this->getCumulativeCostWithUpgrades($level);
        //$cost = $this->getLevelCost($level);

        list($q,$r) = gmp_div_qr($cost, $dps);
        $q = gmp_strval($q);
        $r = gmp_strval($r);

        return (float)($q . '.' . $r);
        //return sprintf('%.02f', (float)($q . '.' . $r));

    }


}