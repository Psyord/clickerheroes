<?php

require 'vendor/autoload.php';

use Kartio\ClickerHeroes\Hero;
use Kartio\ClickerHeroes\HeroNumber;

/*
$n = new HeroNumber('12345');

r($real = $n->getRealValue());
r($n->getShortValue());
r($n->getScientificNotation());

r((float)$n->getScientificNotation() , (float) $real);
*/

$n = new HeroNumber('1.23e6');

r($real = $n->getRealValue());
r($n->getShortValue());
r($n->getScientificNotation());

r((float)$n->getScientificNotation() , (float) $real);