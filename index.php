<?php

require 'vendor/autoload.php';

use Kartio\ClickerHeroes\Hero;
use Kartio\ClickerHeroes\HeroNumber;


$heroData = json_decode(file_get_contents('data/herodata.json'));

//header('Content-Type: text/plain');

$heroes = [];

foreach ($heroData as $row) {

    $damage = new HeroNumber($row->baseDamage);
    $cost = new HeroNumber($row->baseCost);

    $hero = new Hero($row->name, $damage, $cost, $row->upgrades);

    printf("----\n%s\n-----\n" ,$hero->getName());

    hero_print($hero);

    print "\n\n";

    break;

}
/*
printf('Hero: %s', $hero->getName());

print '<table style="border: 1px solid;"><tr><th>Level</th><th>Dps</th><th>Cost</th><th>Eff</th></tr>';

foreach (range(1,50) as $i) {

    printf('<tr><td>%d</td><td>%s</td><td>%s</td><td>%s</td></tr>',
        $i,
        $hero->getDps($i),
        $hero->getCumulativeCostWithUpgrades($i),
        $hero->getEfficiency($i)
    );


}

print '</table>';
*/

/*
foreach (range(1,150) as $i) {

    print $i .' '. $hero->getDps($i).'<br>';

}*/

function hero_print(Hero $hero) {

    printf("% 4s\t% 10s\t% 10s\t% 10s\t%s\n",'Level', 'Cost', 'Dps', 'Up', 'Eff');

    foreach (range(1,200, 1) as $i) {

        printf("% 4s\t% 10s\t% 10s\t% 10s\t%s\n",
            $i,
            new HeroNumber($hero->getCumulativeCostWithUpgrades($i)),
            new HeroNumber($hero->getDps($i)),
            $hero->getUpgradeBonus($i),
            $hero->getEfficiency($i)
        );

    }
}