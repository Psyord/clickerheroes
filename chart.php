<?php

require 'vendor/autoload.php';

use Ghunti\HighchartsPHP\Highchart;
use Kartio\ClickerHeroes\Hero;
use Kartio\ClickerHeroes\HeroNumber;

$chart = new Highchart();

$heroData = json_decode(file_get_contents('data/herodata.json'));

$hero = array_shift($heroData);


$damage = new HeroNumber($hero->baseDamage);
$cost = new HeroNumber($hero->baseCost);

$hero = new Hero($hero->name, $damage, $cost, $hero->upgrades);

$chart->series[0] = array('name' => 'Efficiency');
//$chart->series[1] = array('name' => 'DPS');
//$chart->series[2] = array('name' => 'Cost');
foreach (range(1,501, 1) as $i) {
    $chart->series[0]['data'][] = $hero->getEfficiency($i);

    /*
    $dps = new HeroNumber($hero->getDps($i));
    $chart->series[1]['data'][] = (int)$dps->getRealValue();

    $cost = new HeroNumber($hero->getCumulativeCostWithUpgrades($i));
    $chart->series[2]['data'][] = (int)$cost->getRealValue();
    */
}

$chart->title = array(
    'text' => 'Efficiency of hero (lower is better)',
);
$chart->chart = array(
    'renderTo' => 'container',
    'type' => 'line',

);

?>
<html>
<head>
<title>Basic Line</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php $chart->printScripts(); ?>
</head>
<body>
<div id="container"></div>
<script type="text/javascript"><?php echo $chart->render("chart1"); ?></script>
</body>
</html>