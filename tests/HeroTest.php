<?php

namespace tests;

use Kartio\ClickerHeroes\Hero;
use Kartio\ClickerHeroes\HeroNumber;

class HeroTest extends \PHPUnit_Framework_TestCase {

    public function getBaseHero() {

        $dps = new HeroNumber(5);
        $cost = new HeroNumber(50);
        $upgrades = (object)[];

        return new Hero('TestHero', $dps, $cost, $upgrades);

    }

    public function testDpsValues() {
        $hero = $this->getBaseHero();

        $level = 10;

        $this->assertEquals('50', $hero->getDps($level));

    }

} 