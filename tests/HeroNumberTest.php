<?php

namespace tests;

use Kartio\ClickerHeroes\HeroNumber;

/**
 * @coversDefaultClass \Kartio\ClickerHeroes\HeroNumber
 */
class HeroNumberTest extends \PHPUnit_Framework_TestCase {

    /**
     * @covers \Kartio\ClickerHeroes\HeroNumber::getScientificNotation
     * @covers \Kartio\ClickerHeroes\HeroNumber::parseNumber
     * @covers \Kartio\ClickerHeroes\HeroNumber::getRealValue
     */
    public function testScientificNotation() {

        $notation = '1.234e10';

        $n = new HeroNumber($notation);

        // Parsing back to same format
        $this->assertEquals($notation, $n->getScientificNotation());

        // Actually getting real value
        $this->assertEquals('12340000000', $n->getRealValue());

        $n = new HeroNumber('1e0');
        $this->assertEquals('1', $n->getRealValue());

        $n = new HeroNumber('1e1');
        $this->assertEquals('10', $n->getRealValue());

        $n = new HeroNumber('19e1');
        $this->assertEquals('190', $n->getRealValue());

        $n = new HeroNumber('19.34e2');
        $this->assertEquals((float)'19.34e2', $n->getRealValue());

        $n = new HeroNumber('7.4020527295e74');
        $this->assertEquals((float)'7.4020527295e74', $n->getRealValue());

    }


}