<?php

require 'vendor/autoload.php';

use Kartio\ClickerHeroes\Hero;
use Kartio\ClickerHeroes\HeroNumber;

$heroData = json_decode(file_get_contents('data/herodata.json'));


foreach ($heroData as $order => $row) {

    $time_start = microtime(true);

    $damage = new HeroNumber($row->baseDamage);
    $cost = new HeroNumber($row->baseCost);

    $hero = new Hero($row->name, $damage, $cost, $row->upgrades);

    $data = [
        'heroName' => $hero->getName(),
        'heroOrder' => $order,
        'generated' => (new \DateTime('now UTC'))->format(\DateTime::ISO8601),
        'rows' => []
    ];

    foreach (range(1,2500, 1) as $level) {

        if (($level % 50) == 0) {
            printf("\rProcessing level %d. Time passed %.02f seconds.", $level, microtime(true) - $time_start);
        }

        $cost = new HeroNumber($hero->getCumulativeCostWithUpgrades($level));
        $dps = new HeroNumber($hero->getDps($level));
        $lvl_cost = new HeroNumber($hero->getLevelCost($level));


        $data['rows'][] = [

            'level' => $level,
            'dps_sn' => $dps->getScientificNotation(),
            'cumulativeCost_sn' => $cost->getScientificNotation(),
            'dps' => $dps->getRealValue(),
            'cumulativeCost' => $cost->getRealValue(),
            'efficiency' => $hero->getEfficiency($level),
            'upgradeBonus' => (int)$hero->getUpgradeBonus($level),
            'levelCost_sn' => $lvl_cost->getScientificNotation(),
            'levelCost' => $lvl_cost->getRealValue()

        ];

    }

    $filename = sprintf('data/heroes/%d_%s.json', $order, alterName($hero->getName()));

    file_put_contents($filename, json_encode($data, JSON_PRETTY_PRINT));
    printf("\nHero %s file made in %.02f seconds.\n", $hero->getName(), microtime(true) - $time_start);

    unset($data, $hero);


}


function alterName($name) {

    return strtolower(preg_replace(['/\s+/', '/[\W]/'], ['_', ''], $name));

}